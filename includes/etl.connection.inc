<?php

/**
 * @file
 * ETL database connection functions.
 */

/**
 * Connect to destination database.
 */
function etl_database_connection() {
  $etl_database = array(
    'database' => variable_get('etl_database_database'),
    'username' => variable_get('etl_database_username'),
    'password' => variable_get('etl_database_password'),
    'host' => variable_get('etl_database_host'),
    'driver' => variable_get('etl_database_driver'),
    'port' => variable_get('etl_database_port'),
  );

  try {
    Database::addConnectionInfo('etl_database_connection', 'default', $etl_database);
    return Database::getConnection('default', 'etl_database_connection');
  }
  catch (PDOException $e) {
    watchdog(
      'etl',
      'There was an error trying to connect on destination database. @error',
      array(
        '@error' => $e,
      ),
      WATCHDOG_ERROR
    );
  }
}
