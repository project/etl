<?php

/**
 * @file
 * ETL administration settings.
 */

/**
 * Admin form for general configuration.
 */
function etl_settings_form($form, &$form_state) {
  $form = array();

  $form['batch'] = array(
    '#type' => 'fieldset',
    '#title' => t('Batch'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['batch']['etl_batch_package_size'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('etl_batch_package_size', 30),
    '#title' => t('Package size'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * General settings page for entities.
 */
function etl_entity_settings_form($form, &$form_state, $entity) {
  $destination_database = etl_check_destination_database_connection();

  if (!$destination_database) {
    drupal_set_message(t('Unable to connect to destination database. Please check your configuration.'), 'error');
    return array();
  }

  $entities = etl_load_entities();

  $entity_info = entity_get_info($entities[$entity]['entity']);

  $source_records = etl_get_source_entity_records($entity_info, $entities[$entity]['bundle']);

  $info = array(
    'entity' => $entity,
    'entities' => $entities,
    'entity_info' => $entity_info,
  );

  $table_exists = etl_check_destination_table_existence($destination_database, $entities[$entity]['destination_table']);

  // Builds the configuration form if needed.
  if (!empty($form_state['storage'])) {
    $confirm_form = confirm_form(
      $form,
      t('Are you sure you want to reset the %entity table?', array(
        '%entity' => $entity,
      )),
      "admin/config/development/etl/{$entity}",
      t('All of the stored data will be lost.'),
      t('Reset'),
      t('Cancel')
    );
    $confirm_form['#submit'] = $form_state['triggering_element']['#submit'];
    return $confirm_form;
  }

  $destination_records = array();
  if ($table_exists) {
    $destination_records = etl_get_destination_records($destination_database, $entities[$entity]['destination_table'], $info);
  }

  $difference_records = array_diff($source_records, $destination_records);

  $exported_source_records_number = count($source_records) - count($difference_records);
  $source_records_number = count($source_records);

  $etl_batch_package_size = variable_get('etl_batch_package_size', 30);

  $batch_packages = ($difference_records) ? ceil(count($difference_records) / $etl_batch_package_size) : 0;

  $form = array();

  $form['entity'] = array(
    '#type' => 'fieldset',
    '#title' => t('Information'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['entity']['source'] = array(
    '#markup' => "There are <strong>{$exported_source_records_number}</strong> of <strong>{$source_records_number}</strong> records exported.<br/><br/>",
  );

  $form['entity']['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset'),
    '#access' => ($exported_source_records_number <= 0) ? FALSE : TRUE,
    '#submit' => array('etl_reset_submit'),
  );

  $form['entity']['export'] = array(
    '#type' => 'submit',
    '#value' => t('Export'),
    '#access' => $table_exists,
    '#submit' => array('etl_export_submit'),
  );

  $info += array(
    'difference_records' => $difference_records,
    'batch_package_size' => $etl_batch_package_size,
    'batch_packages' => $batch_packages,
  );

  $form['entity']['info'] = array(
    '#type' => 'value',
    '#value' => $info,
  );

  $form['#action'] = url("admin/config/development/etl/{$entity}/confirm");

  etl_mapping_config_form($form, $info);

  drupal_add_tabledrag('etl-mapping', 'order', 'sibling', 'etl-mapping-weight');

  return $form;
}

/**
 * Return the records from destination table.
 *
 * @param object $destination_database
 *   The destination database object connection.
 * @param string $destination_table
 *   The destination table name.
 * @param array $info
 *   Entity information to be configured.
 */
function etl_get_destination_records($destination_database, $destination_table, $info) {
  $source_fields_info = etl_get_source_fields_info($info);

  $settings = etl_entity_mapping_settings($info['entity']);

  foreach ($source_fields_info as $machine_name => $fields_info) {
    if (isset($fields_info['primary_key'])) {
      $destination_id = $settings[$machine_name]['label'];
      break;
    }
  }

  $query = $destination_database->select($destination_table)
    ->fields($destination_table, array($destination_id));

  return $query->execute()->fetchCol();
}

/**
 * Return the records from source entity type.
 *
 * @param string $entity_info
 *   The entity information.
 * @param string $bundle
 *   Used when export specific bundles of an entity.
 */
function etl_get_source_entity_records($entity_info, $bundle = NULL) {
  $query = db_select($entity_info['base table'])
    ->fields($entity_info['base table'], array($entity_info['entity keys']['id']));

  if ($bundle) {
    $query->condition($entity_info['bundle keys']['bundle'], $bundle, '=');
  }

  return $query->execute()->fetchCol();
}

/**
 * Implements hook_form_submit().
 */
function etl_reset_submit($form, &$form_state) {
  $destination_database = etl_check_destination_database_connection();

  $info = isset($form_state['storage']) ? $form_state['storage']['info'] : $form_state['values']['info'];

  $destination_table = $info['entities'][$info['entity']]['destination_table'];

  $table_exists = etl_check_destination_table_existence($destination_database, $destination_table);

  // Builds the confirmation form if the table exists.
  if (empty($form_state['storage']) && $table_exists) {
    $form_state['storage'] = $form_state['values'];
    $form_state['rebuild'] = TRUE;
    return;
  }

  $destination_database->truncate($destination_table)->execute();
  $form_state['redirect'] = "admin/config/development/etl/{$info['entity']}";
}

/**
 * Implements hook_form_submit().
 */
function etl_export_submit($form, &$form_state) {
  module_load_include('inc', 'etl', 'includes/etl.export');
  $info = $form_state['values']['info'];

  etl_entity_export($info);
}

/**
 * Implements the field mapping configuration form.
 *
 * @param array $form
 *   Form to be updated with mappings table configuration.
 * @param array $info
 *   Entity information to be configured.
 */
function etl_mapping_config_form(&$form, $info) {
  // Build the sortable table header.
  $header = array(
    'custom' => t('Custom Field'),
    'label' => t('Label'),
    'machine_name' => t('Machine name'),
    'destination_column' => t('Destination Column'),
    'type' => t('Type'),
    'size' => t('Size'),
    'export' => t('Export?'),
    'weight' => t('Weight'),
  );

  $rows = etl_get_mapping_config_rows($info);

  $form[$info['entity']] = array(
    '#tree' => TRUE,
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No mappings available.'),
    '#js_select' => TRUE,
    '#attributes' => array(
      'id' => 'etl-mapping',
    ),
  );

  $form['export_configuration'] = array(
    '#type' => 'submit',
    '#value' => t('Save Configuration'),
    '#submit' => array('etl_mapping_config_form_submit'),
  );
}

/**
 * Implements hook_form_submit().
 */
function etl_mapping_config_form_submit($form, &$form_state) {
  $destination_database = etl_check_destination_database_connection();
  $info = isset($form_state['storage']['values']) ? $form_state['storage']['values']['info'] : $form_state['values']['info'];

  $destination_table = $info['entities'][$info['entity']]['destination_table'];

  $table_exists = etl_check_destination_table_existence($destination_database, $destination_table);

  // Builds the confirmation form if the table already exists.
  if (empty($form_state['storage']) && $table_exists) {
    $form_state['storage'] = $form_state;
    $form_state['rebuild'] = TRUE;
    return;
  }

  // @codingStandardsIgnoreStart
  $values = isset($form_state['storage']) ? $form_state['storage']['input'] : $form_state['input'];
  // @codingStandardsIgnoreEnd

  $source_fields_info = etl_get_source_fields_info($info);

  $entity = $info['entity'];

  $settings = array();

  foreach ($source_fields_info as $machine_name => $field_info) {
    $settings[$machine_name] = array(
      'label' => $values["{$entity}_{$machine_name}_label"],
      'type' => $values["{$entity}_{$machine_name}_type"],
      'size' => NULL,
      'export' => 0,
      'weight' => $values["{$entity}_{$machine_name}_weight"],
      'custom' => $values["{$entity}_{$machine_name}_weight"],
    );

    if (isset($values["{$entity}_{$machine_name}_size"]) && !empty($values["{$entity}_{$machine_name}_size"])) {
      $settings[$machine_name]['size'] = $values["{$entity}_{$machine_name}_size"];
    }

    if (isset($values["{$entity}_{$machine_name}_export"]) || isset($field_info['primary_key'])) {
      $settings[$machine_name]['export'] = 1;
    }
  }

  uasort($settings, function ($a, $b) {
    return $a["weight"] - $b["weight"];
  });

  if (empty($settings)) {
    drupal_set_message(t('There was no field selected to export, please select at least one field.'), 'error');
    return;
  }

  etl_save_mapping_settings($info['entity'], $settings);

  if (etl_check_duplicated_labels($info)) {
    etl_create_destination_table($settings, $source_fields_info, $info);
  }

  drupal_set_message(t("The configuration options have been saved."));
  $form_state['redirect'] = "admin/config/development/etl/{$info['entity']}";
}

/**
 * Get the mapping configuration rows.
 *
 * @param array $info
 *   Entity information to be configured.
 */
function etl_get_mapping_config_rows($info) {
  $source_fields_info = etl_get_source_fields_info($info);

  $settings = etl_entity_mapping_settings($info['entity']);

  $rows = array();

  $mapping_data = array();

  foreach ($source_fields_info as $machine_name => $field_info) {
    $type = isset($field_info['schema']['type']) ? $field_info['schema']['type'] : '';
    $size = (isset($field_info['schema']['size'])) ? ':' . $field_info['schema']['size'] : '';
    $mapping_data[$machine_name] = array(
      'label' => etl_sanitize_label($field_info['label']),
      'type' => "{$type}{$size}",
      'size' => NULL,
      'export' => 0,
      'weight' => 0,
    );

    if (isset($settings[$machine_name])) {
      $mapping_data[$machine_name] = array(
        'label' => $settings[$machine_name]['label'],
        'type' => $settings[$machine_name]['type'],
        'size' => $settings[$machine_name]['size'],
        'weight' => $settings[$machine_name]['weight'],
        'export' => $settings[$machine_name]['export'],
      );
    }

    $mapping_data[$machine_name]['custom'] = $field_info['custom'];
    $mapping_data[$machine_name]['drupal_label'] = $field_info['label'];
    if (isset($field_info['primary_key'])) {
      $mapping_data[$machine_name]['primary_key'] = TRUE;
    }
  }

  uasort($mapping_data, function ($a, $b) {
    return $a["weight"] - $b["weight"];
  });

  $mapping_name = $info['entity'];
  foreach ($mapping_data as $machine_name => $field_info) {
    $mapping_key = "{$mapping_name}_{$machine_name}";

    $rows[$mapping_key] = array(
      'data' => array(
        'custom' => ($field_info['custom']) ? t("Custom") : t('Default'),
        'label' => $field_info['drupal_label'],
        'machine_name' => $machine_name,
        'destination_column' => array(
          'data' => array(
            '#id' => "{$mapping_key}_label",
            '#name' => "{$mapping_key}_label",
            '#type' => 'textfield',
            '#maxlength' => 59,
            '#value' => $field_info['label'],
          ),
        ),
        'type' => array(
          'data' => array(
            '#id' => "{$mapping_key}_type",
            '#name' => "{$mapping_key}_type",
            '#type' => 'select',
            '#options' => array(
              'serial:tiny' => t('serial:tiny'),
              'serial:small' => t('serial:small'),
              'serial:medium' => t('serial:medium'),
              'serial:big' => t('serial:big'),
              'serial' => t('serial'),
              'int:tiny' => t('int:tiny'),
              'int:small' => t('int:small'),
              'int:medium' => t('int:medium'),
              'int:big' => t('int:big'),
              'int' => t('int'),
              'float:tiny' => t('float:tiny'),
              'float:small' => t('float:small'),
              'float:medium' => t('float:medium'),
              'float:big' => t('float:big'),
              'float' => t('float'),
              'numeric' => t('numeric'),
              'varchar' => t('varchar'),
              'char' => t('char'),
              'text:tiny' => t('text:tiny'),
              'text:small' => t('text:small'),
              'text:medium' => t('text:medium'),
              'text:big' => t('text:big'),
              'text' => t('text'),
            ),
            '#value' => $field_info['type'],
          ),
        ),
        'size' => array(
          'data' => array(
            '#id' => "{$mapping_key}_size",
            '#name' => "{$mapping_key}_size",
            '#type' => 'textfield',
            '#maxlength' => 5,
            '#size' => 3,
            '#value' => $field_info['size'],
          ),
        ),
        'export' => array(
          'data' => array(
            '#id' => "{$mapping_key}_export",
            '#name' => "{$mapping_key}_export",
            '#type' => 'checkbox',
          ),
        ),
        'weight' => array(
          'data' => array(
            '#id' => "{$mapping_key}_weight",
            '#name' => "{$mapping_key}_weight",
            '#type' => 'textfield',
            '#maxlength' => 5,
            '#size' => 2,
            '#value' => $field_info['weight'],
          ),
        ),
      ),
      'class' => array('draggable'),
    );

    $rows[$mapping_key]['data']['weight']['data']['#attributes']['class'][] = 'etl-mapping-weight';

    if (!empty($field_info['export'])) {
      $rows[$mapping_key]['data']['export']['data']['#attributes'] = array('checked' => 'checked');
    }

    if (isset($field_info['primary_key'])) {
      $rows[$mapping_key]['data']['export']['data']['#attributes'] = array(
        'checked' => 'checked',
        'disabled' => 'disabled',
      );
    }

  }

  return $rows;
}

/**
 * Retrieve source entity fields and properties.
 *
 * @param array $info
 *   Entity information to be configured.
 */
function etl_get_source_fields_info($info) {
  $el_entity_info = $info['entities'][$info['entity']];
  $entity = $el_entity_info['entity'];

  $entity_info = entity_get_info($entity);

  // Get all the property schema information.
  $entity_schema = drupal_get_schema($entity_info['base table']);
  $properties_types = $entity_schema['fields'];

  $entity_property_info = entity_get_property_info($entity);

  $bundle = $el_entity_info['bundle'];

  $extra_fields = isset($el_entity_info['extra_columns']) ? $el_entity_info['extra_columns'] : array();

  $entity_bundle_fields = array();

  $field_read_fields = array(
    'entity_type' => $entity,
  );

  if ($bundle) {
    $entity_bundle_fields[$bundle] = $entity_property_info['bundles'][$bundle];
    $field_read_fields['bundle'] = $bundle;
  }
  else {
    $entity_bundle_fields = $entity_property_info['bundles'];
  }

  // Get all the fields schema information.
  $fields_types = field_read_fields($field_read_fields);

  $entity_fields = array();

  // Sets all fields from all bundles of that entity.
  foreach ($entity_bundle_fields as $bundle) {
    foreach ($bundle['properties'] as $machine_name => $field_info) {
      if (!isset($fields_types[$machine_name])) {
        continue;
      }

      $field_schema = reset($fields_types[$machine_name]['columns']);
      unset($field_schema['not null']);
      unset($field_schema['unsigned']);
      $entity_fields[$machine_name] = array(
        'label' => $field_info['label'],
        'schema' => $field_schema,
        'custom' => FALSE,
      );
    }
  }

  // Sets all properties of that field.
  foreach ($properties_types as $machine_name => $schema_info) {
    $property_label = $machine_name;
    if (isset($entity_property_info['properties'][$machine_name]['label'])) {
      $property_label = $entity_property_info['properties'][$machine_name]['label'];
    }

    unset($schema_info['not null']);
    unset($schema_info['unsigned']);
    $entity_fields[$machine_name] = array(
      'label' => $property_label,
      'schema' => $schema_info,
      'custom' => FALSE,
    );
  }

  // Sets the extra fields created via hook_etl_load_entities_info.
  foreach ($extra_fields as $machine_name) {
    $entity_fields[$machine_name] = array(
      'label' => $machine_name,
      'schema' => array(),
      'custom' => TRUE,
    );
  }

  // Set the property primary key.
  $primary_key = reset($entity_schema['primary key']);
  $entity_fields[$primary_key]['primary_key'] = TRUE;

  return $entity_fields;
}

/**
 * Sanitize the field label for destination column.
 *
 * @param string $string
 *   The label text to be sanitized.
 */
function etl_sanitize_label($string) {
  // Turn initial letters into capital letters.
  $string = ucwords($string);
  // Replaces all spaces with hyphens.
  $string = str_replace(' ', '', $string);
  // Removes special chars.
  $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
  // Restrict maxlength for 59 characters.
  $string = substr($string, 0, 59);

  return $string;
}

/**
 * Check destination table existence.
 *
 * @param object $destination_database
 *   The destination connection database object.
 * @param string $table
 *   The destination table name.
 */
function etl_check_destination_table_existence($destination_database, $table) {
  $table_exists = $destination_database->schema()->tableExists($table);

  if (!$table_exists) {
    drupal_set_message(t('There is no destination table, please save the mapping below.'), 'warning', FALSE);
    return FALSE;
  }

  return TRUE;
}

/**
 * Check destination database connection.
 */
function etl_check_destination_database_connection() {
  module_load_include('inc', 'etl', 'includes/etl.connection');
  $destination_database = etl_database_connection();

  if ($destination_database) {
    return $destination_database;
  }

  return FALSE;
}

/**
 * Check for duplicated labels.
 *
 * @param array $info
 *   Entity information to be configured.
 */
function etl_check_duplicated_labels($info) {
  $settings = etl_entity_mapping_settings($info['entity']);

  $mapping = array();

  foreach ($settings as $machine_name => $field) {
    if (empty($field['export'])) {
      continue;
    }
    $mapping[$machine_name] = $field['label'];
  }

  $duplicated = array_diff_assoc($mapping, array_unique($mapping));

  if (empty($duplicated)) {
    return TRUE;
  }

  $duplicated = implode(", ", array_values($duplicated));

  drupal_set_message(t("The following labels are duplicated and won't be exported properly: @d", array('@d' => $duplicated)), 'error');

  return FALSE;
}

/**
 * Create the destination table according to mapping.
 *
 * @param array $mapping
 *   The mapping fields configuration.
 * @param array $fields_info
 *   Fields and properties schema information.
 * @param array $info
 *   Entity information to be configured.
 */
function etl_create_destination_table($mapping, $fields_info, $info) {
  $destination_database = etl_check_destination_database_connection();

  if (!$destination_database) {
    return;
  }

  $destination_table = $info['entities'][$info['entity']]['destination_table'];

  if (etl_check_destination_table_existence($destination_database, $destination_table)) {
    $destination_database->schema()->dropTable($destination_table);
  }

  $fields_schema = array();

  foreach ($mapping as $machine_name => $field) {
    if (empty($field['export'])) {
      continue;
    }

    $typesize = explode(':', $field['type']);
    $fields_info[$machine_name]['schema']['type'] = $typesize[0];

    // Prevents field schema overriding the user choice.
    unset($fields_info[$machine_name]['schema']['size']);

    if (isset($typesize[1])) {
      $fields_info[$machine_name]['schema']['size'] = $typesize[1];
    }

    if (!empty($field['size'])) {
      $fields_info[$machine_name]['schema']['length'] = $field['size'];
    }

    $fields_schema[$field['label']] = $fields_info[$machine_name]['schema'];
  }

  try {
    $destination_database->schema()->createTable($destination_table, array('fields' => $fields_schema));
  }
  catch (PDOException $e) {
    $message = 'There was an error trying to create the destination table.';
    watchdog(
      'etl',
      '@message @error',
      array('@message' => $message, '@error' => $e->getMessage()),
      WATCHDOG_ERROR
    );
    drupal_set_message(t('@message @error', array('@message' => $message, '@error' => $e->getMessage())), 'error', FALSE);
    return FALSE;
  }
}
