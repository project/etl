<?php

/**
 * @file
 * ETL export data functions.
 */

/**
 * Initialize full or continue export process.
 *
 * @param array $info
 *   Entity information to be exported.
 */
function etl_entity_export($info) {
  $difference_records = $info['difference_records'];

  $information = array(
    'entity' => $info['entity'],
    'entity_key' => $info['entity_info']['entity keys']['id'],
    'entity_type' => $info['entities'][$info['entity']]['entity'],
    'destination_table' => $info['entities'][$info['entity']]['destination_table'],
    'batch_package_size' => $info['batch_package_size'],
    'batch_packages' => $info['batch_packages'],
  );

  $batch = array(
    'operations' => array(),
    'title' => t('Processing @entity packaging export.', array('@entity' => $information['entity'])),
    'init_message' => t('Creating @entity packages.', array('@entity' => $information['entity'])),
    'progress_message' => t('Processed @current out of @total packages.'),
    'error_message' => t('Export of @entity package has encountered an error.', array('@entity' => $information['entity'])),
    'file' => drupal_get_path('module', 'etl') . '/includes/etl.export.inc',
  );

  for ($package = 0; $package < $information['batch_packages']; $package++) {
    $offset = $package * $information['batch_package_size'];

    $entities = array_slice($difference_records, $offset, $information['batch_package_size']);

    $batch['operations'][] = array(
      'etl_package_export_batch',
      array($entities, $information, $package),
    );
  }

  batch_set($batch);
  batch_process("admin/config/development/etl/{$information['entity']}");
}

/**
 * Batch packages export.
 *
 * @param array $entities
 *   List of entities IDs to be exported.
 * @param array $info
 *   Entity information to be configured.
 * @param int $package
 *   Current package number.
 * @param array $context
 *   The array gathers batch context information about the execution.
 */
function etl_package_export_batch($entities, $info, $package, &$context) {
  $batch = array(
    'operations' => array(),
    'title' => t('Processing @entity export.', array('@entity' => $info['entity'])),
    'init_message' => t('Exporting @entity started.', array('@entity' => $info['entity'])),
    'progress_message' => t('Processed @current out of @total records.'),
    'error_message' => t('Export of @entity has encountered an error.', array('@entity' => $info['entity'])),
    'file' => drupal_get_path('module', 'etl') . '/includes/etl.export.inc',
  );

  foreach ($entities as $entity) {
    $batch['operations'][] = array(
      'etl_entity_export_batch',
      array($entity, $info, $package),
    );
  }

  batch_set($batch);
}

/**
 * Batch entity export.
 *
 * @param array $entity
 *   The entity ID to be exported.
 * @param array $info
 *   Entity information to be configured.
 * @param int $package
 *   Current package number.
 * @param array $context
 *   The array gathers batch context information about the execution.
 */
function etl_entity_export_batch($entity, $info, $package, &$context) {
  $context['message'] = "Exporting {$package} of {$info['batch_packages']} packages.";

  etl_prepare_export($entity, $info);
}

/**
 * Prepare entity export.
 *
 * @param array $entity
 *   The entity ID to be exported.
 * @param array $info
 *   Entity information to be configured.
 */
function etl_prepare_export($entity, $info) {
  $entity_settings_name = $info['entity'];
  $entity_mapping = etl_entity_mapping_settings($entity_settings_name);

  $entity_data = etl_entity_load_data($entity, $info, $entity_mapping);

  if ($entity_data) {
    etl_export($entity_data, $info, $entity_mapping);
  }
}

/**
 * Check the existence data and submit.
 *
 * @param array $entity_data
 *   The entity ID information.
 * @param array $info
 *   Entity information to be configured.
 * @param array $entity_mapping
 *   Configured mapping between source and destination.
 */
function etl_export($entity_data, $info, $entity_mapping) {
  module_load_include('inc', 'etl', 'includes/etl.connection');
  $destination_database = etl_database_connection();

  if (!$destination_database) {
    return array();
  }

  $destination_id = $entity_mapping[$info['entity_key']]['label'];

  try {
    etl_multiple_values_fields($destination_database, $entity_data, $destination_id);

    $destination_database->merge($info['destination_table'])
      ->key(array($destination_id => $entity_data[$destination_id]))
      ->fields($entity_data)
      ->execute();
  }
  catch (PDOException $e) {
    $message = 'There was an error trying to merge data on destination database.';
    watchdog(
      'etl',
      '@message @error',
      array('@message' => $message, '@error' => $e->getMessage()),
      WATCHDOG_ERROR
    );
    drupal_set_message(t('@message @error', array('@message' => $message, '@error' => $e->getMessage())), 'error', FALSE);
    return FALSE;
  }

}

/**
 * Treat and parse entity data values.
 *
 * @param array $entity_id
 *   The entity ID information.
 * @param array $info
 *   Entity information to be configured.
 * @param array $entity_mapping
 *   Configured mapping between source and destination.
 */
function etl_entity_load_data($entity_id, $info, $entity_mapping) {
  $wrapper = entity_metadata_wrapper($info['entity_type'], $entity_id);

  $entity_data = array();

  foreach ($entity_mapping as $machine_name => $field_info) {
    if (empty($field_info['export'])) {
      continue;
    }
    if (isset($wrapper->{$machine_name})) {
      $entity_data[$field_info['label']] = etl_get_field_parser_value($machine_name, $wrapper->{$machine_name}, $wrapper);
    }
  }

  // Allow other modules change the entity data before export.
  drupal_alter('etl_entity_load_data', $entity_data, $info);

  return $entity_data;
}

/**
 * Return the readable and denormalized field value.
 *
 * @param string $machine_name
 *   The entity field machine name.
 * @param object $field
 *   The field entity wrapper.
 * @param object $wrapper
 *   The entire entity wrapper.
 */
function etl_get_field_parser_value($machine_name, $field, $wrapper) {
  $array_values = array();

  switch ($field->type()) {
    case 'date':
      $field_value = date('Y-m-d H:i:s', $field->value());
      break;

    case 'field_item_image':
      $field_value = $field->value()['filename'];
      break;

    case 'list<field_item_file>':
      foreach ($field->value() as $item) {
        $array_values[] = $item['filename'];
      }
      $field_value = $array_values;
      break;

    case 'list<text>':
      foreach ($field->value() as $item) {
        $array_values[] = $item;
      }
      $field_value = $array_values;
      break;

    case 'safeword_field':
      $field_value = $field->value()['machine'];
      break;

    case 'node':
    case 'user':
    case 'taxonomy_term':
      if (is_object($field->value())) {
        $field_value = $field->label();
      }
      break;

    case 'list<user>':
    case 'list<node>':
    case 'list<taxonomy_term>':
    case 'list<field_collection_item>':
      foreach ($field as $item) {
        $array_values[] = $item->label();
      }
      $field_value = $array_values;
      break;

    default:
      $field_value = $field->value();

      if (is_object($field_value)) {
        $field_value = $field->label();
      }
      break;
  }

  drupal_alter('etl_get_field_parser_value', $field_value, $machine_name, $field, $wrapper);

  if ($field_value && !is_array($field_value) && !is_object($field_value)) {
    $field_value = trim($field_value);
  }

  return $field_value;
}

/**
 * Create table and values for multiple value fields.
 *
 * @param object $destination_database
 *   The destination database connection.
 * @param array $entity
 *   Entity information to be configured.
 * @param string $destination_id
 *   The column ID from destination table.
 */
function etl_multiple_values_fields($destination_database, &$entity, $destination_id) {
  foreach ($entity as $field => $values) {
    if (!is_array($values)) {
      continue;
    }

    // TODO: Render object values to export.
    if (is_object(reset($values))) {
      return;
    }

    $entity[$field] = implode(', ', $values);
  }
}
