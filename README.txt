INTRODUCTION
------------

The ETL (Extract, Transform and Load) module gives the ability to export and
denormalize any kind of content. Basically you are able to select content types
or custom entities to be exported into a destination database. It also gives you
the possibility to alter the data before persist it.


MAINTAINERS
-----------

Current maintainers:
  * Gabriel Machado Santos - https://www.drupal.org/u/gabrielmachadosantos

This project has been sponsored by:
  * CI&T - CI&T has more experience than anyone else in leading Drupal
    implementations for multi-brand enterprises. When you need to provide better
    more timely, and relevant web experiences for your customers, CI&T takes
    you where you need to be. (http://drupal.org/node/1530378)


REQUIREMENTS
------------

This module requires the following modules:

  * Entity (https://drupal.org/project/entity)


INSTALLATION
------------

No special installation requirements.


CONFIGURATION
------------

Mandatory configuration:

* Set up the desired destination database on settings.php:
  $conf['etl_database_username'] = 'username';
  $conf['etl_database_password'] = 'password';
  $conf['etl_database_database'] = 'database';
  $conf['etl_database_host'] = 'localhost';
  $conf['etl_database_driver'] = 'pgsql';
  $conf['etl_database_port'] = '';

* hook_etl_load_entities_info();
  It is required to list all the desired entities to be exported.

  /**
   * Implements hook_etl_load_entities_info().
   */
  function MYMODULE_etl_load_entities_info() {
    return array(
      'article' => array(
        'entity' => 'node',
        'destination_table' => 'article',
        'bundle' => 'article',
      ),
    );
  }

Useful hooks to alter entities data:

* hook_etl_entity_load_data_alter(&$entity_data, $info);
  Gives you the possibility to change the entire data before it is persisted.

* hook_etl_get_field_parser_value_alter(&$field_value, $machine_name, $field,
  $wrapper);
  Alter a specific field more granular.
