<?php

/**
 * @file
 * Hooks provided by ETL module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Define ETL entities.
 *
 * This hook is required in order to add entities.
 *
 * @return array
 *   An array of information about the module's provided resources.
 *   The array contains a sub-array for each entity, with the entity name as
 *   the key. Entities names may only contain lowercase alpha-numeric characters
 *   and underscores.
 *   Possible attributes for each sub-array are:
 *   - entity: The type of the entity. Required.
 *   - destination_table: The name of the destination table. Required.
 *   - bundle: The default bundle of the related entity. It can be set as NULL
 *     for custom entities. Required.
 *   - extra_columns: An array of custom columns to be added to the entity.
 *     The values can be via hook_etl_entity_load_data_alter(). Optional.
 *
 * @see etl_load_entities()
 */
function hook_etl_load_entities_info() {
  return array(
    'article' => array(
      'entity' => 'node',
      'destination_table' => 'article',
      'bundle' => 'article',
      'extra_columns' => array('column1', 'column2'),
    ),
  );
}

/**
 * Alter available entity data information.
 *
 * @param array $entity_data
 *   Entity loaded data information already denormalized.
 * @param array $info
 *   Entity information as defined in hook_etl_load_entities_info().
 *
 * @see hook_etl_entity_load_data_alter()
 */
function hook_etl_entity_load_data_alter(&$entity_data, $info) {
  switch ($info['entity']) {
    case 'article':
      // Limite the title string characters.
      $entity_data['Title'] = substr($entity_data['Title'], 0, 50);
      break;
  }
}

/**
 * Alter available entity data information per field.
 *
 * @param string $field_value
 *   The entity field value denormalized information.
 * @param string $machine_name
 *   The field machine name.
 * @param object $field
 *   The entity_metadata_wrapper field information.
 * @param object $wrapper
 *   The entity_metadata_wrapper for entire entity.
 *
 * @see hook_etl_get_field_parser_value_alter()
 */
function hook_etl_get_field_parser_value_alter(&$field_value, $machine_name, $field, $wrapper) {
  if ('user' == $field->type()) {
    $field_value = $field->raw();
  }
}

/**
 * @} End of "addtogroup hooks".
 */
